﻿using UnityEngine;
using System.Collections;

public class EnemyFactory : MonoBehaviour {
	Vector3 spawnPoint;
	float spawnRadius;
	EnemyType basicEnemy;//prefabs
	public enum EnemyTypeMold{basicEnemy,playerAttacker}
	// Use this for initialization
	public GameObject createEnemy(EnemyTypeMold type){
		switch (type) {
		case EnemyTypeMold.basicEnemy:
			return SpawnEnemy ();

		}
		return null;
	}
	public GameObject SpawnEnemy(){
		GameObject enemyClone = (GameObject)Instantiate (basicEnemy.prefab, UpdateSpawnPos (spawnPoint, spawnRadius), Quaternion.identity);
		//NetworkServer.Spawn (enemyClone);
		enemyClone.GetComponent<BasicEnemy>().setAttributes(5, 10, 1, 5, BasicEnemy.targetStateEnum.targetPlayer);
		return enemyClone;

	}
	Vector3 UpdateSpawnPos(Vector3 centreSpawnPos, float range){
		Vector3 spawnPos;
		spawnPos.x = centreSpawnPos.x + (Random.Range (-range, range));
		spawnPos.z = centreSpawnPos.z + (Random.Range (-range, range));
		spawnPos.y = centreSpawnPos.y;
		return spawnPos;
	}
}
